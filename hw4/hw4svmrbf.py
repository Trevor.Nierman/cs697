# Trevor Nierman
# R957F692
# CS697 HW4

# hw4.py

import pandas
from sklearn import svm
from sklearn.model_selection import cross_val_score
from numpy import mean

## Read input data into correct vectors ##

inputData = pandas.read_csv('zip.train', delim_whitespace=True, skipinitialspace=True, header=None)

xTrain = inputData.iloc[:, 1:]
yTrain = inputData.iloc[:, 0]

inputData = pandas.read_csv('zip.test', delim_whitespace=True, skipinitialspace=True, header=None)

xTest = inputData.iloc[:, 1:]
yTest = inputData.iloc[:, 0]


## Create model ##

clf=svm.SVC(kernel='rbf', gamma='auto')


## Determine C value ##

print("Determining best C value:")
print("")


clf.C = 0.001

print("Current C: %.3f" % clf.C)

cvs = cross_val_score(estimator=clf, X=xTrain, y=yTrain, cv=5) 
cvs = mean(cvs)

bestCVS = cvs
globalC = clf.C

print("Average cross validation accuracy: %.7f" % cvs)
print("")

cValues = [0.01, 0.10, 1.00, 10.00, 100.00, 1000.00]

for c in cValues:
    clf.C = c
    cvs = cross_val_score(estimator=clf, X=xTrain, y=yTrain, cv=5) 
    cvs = mean(cvs)

    if cvs > bestCVS:
        globalC = clf.C
        bestCVS = cvs

    print("Current C: %.3f" % clf.C)
    print("Average cross validation accuracy: %.7f" % cvs)


print("")
print("\t--- Results ---")
print("")
print("Most accurate C value: %.3f" % globalC)
print("Cross validation score: %.7f" % bestCVS)


## Train SVM ##

print("")
print("Training model")

clf.C = globalC
clf.fit(xTrain, yTrain)
testResults = clf.score(xTest, yTest)

print("")
print("Model trained")
print("Test accuracy: %.3f" % testResults)

