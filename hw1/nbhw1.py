import pandas
from sklearn.naive_bayes import GaussianNB

### Reshape test data into vector

rawData = pandas.read_csv('spam-testing.txt', header=None)

testDataVector = rawData.values
# testDataVector = testDataVector.reshape(rawData.shape[0])

### Reshape test labels into vector

rawData = pandas.read_csv('test-labels.txt', header=None)

testLabelVector = rawData.values
testLabelVector = testLabelVector.reshape(rawData.shape[0])

### Reshape training data into vector

rawData = pandas.read_csv('spam-training.txt', header=None)

trainingDataVector = rawData.values
# trainingDataVector = trainingDataVector.reshape(rawData.shape[0])

### Reshape training labels into vector

rawData = pandas.read_csv('train-labels.txt', header=None)

trainingLabelVector = rawData.values
trainingLabelVector = trainingLabelVector.reshape(rawData.shape[0])

### Spam data calculations

gnb = GaussianNB()
dataPrediction = gnb.fit(trainingDataVector, trainingLabelVector).predict(testDataVector)

totalDataPoints = float(trainingDataVector.shape[0])
missedDataPoints = float((testLabelVector != dataPrediction).sum())
percentError = (missedDataPoints / totalDataPoints) * 100

print("Total data points: %d" % totalDataPoints)
print("Mislabelled data points: %d" % missedDataPoints)
print("Percentage incorrect: %f" % percentError)
