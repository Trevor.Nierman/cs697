import pandas as pd
from sklearn import linear_model
from sklearn.metrics import mean_squared_error

## Read data into correct vectors ##

inputData = pd.read_csv('./boston_housing.data', delimiter=' ' , header=None)

X_train = inputData.iloc[:100, :13]
Y_train = inputData.iloc[:100, -1]

X_test = inputData.iloc[100:, :13]
Y_test = inputData.iloc[100:, -1]

## Train regression model as lasso (without cross-validation) ##

lassoModel = linear_model.Lasso(alpha = 10.0)
lassoModel.fit(X_train, Y_train)
Y_pred = lassoModel.predict(X_test)

# Print results
print("")
print("----- Results (without cross-validation) -----")
print("Mean squared error: %.2f" % mean_squared_error(Y_test, Y_pred))

numberZeros = 0
for element in lassoModel.coef_:
    if element == 0.:
        numberZeros += 1
        
print("Number of zero coefficients: %d" % numberZeros)
print("")

print("Coefficients:")
print(lassoModel.coef_)
print("")

## Train regression model with cross validation ##

lassoCVModel = linear_model.LassoCV (alphas=[0.01, 0.1, 1.0, 10.0, 100.0], cv=5, random_state=0)
lassoCVModel.fit(X_train, Y_train)
Y_pred = lassoCVModel.predict(X_test)

# Print results
print("----- Results (with cross-validation) -----")
print("Mean squared error: %.2f" % mean_squared_error(Y_test, Y_pred))

numZeros = 0
for element in lassoCVModel.coef_:
    if element == 0.:
        numZeros += 1

print("Number of zero coefficients: %d" % numZeros)
print("alpha= %.2f" % lassoCVModel.alpha_)
print("")

print("Coefficients:")
print(lassoCVModel.coef_)
print("")