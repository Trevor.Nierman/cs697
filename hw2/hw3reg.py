import pandas as pd
from sklearn import linear_model
from sklearn.metrics import mean_squared_error

## Read data into correct vectors ##

inputData = pd.read_csv('boston_housing.data', delimiter=' ' , header=None)

X_train = inputData.iloc[:400, :13]
Y_train = inputData.iloc[:400, -1]

X_test = inputData.iloc[400:, :13]
Y_test = inputData.iloc[400:, -1]

## Train regression model ##

regressionModel = linear_model.LinearRegression()
regressionModel.fit(X_train, Y_train)
Y_pred = regressionModel.predict(X_test)

## Print results ##

print("")
print("----- Results -----")

print("Prediction:")
print(Y_pred)
print("")

print("Mean squared error: %.2f" % mean_squared_error(Y_test, Y_pred))
print("")

print("Coefficients: ")
print(regressionModel.coef_)
print("")