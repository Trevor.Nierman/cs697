import pandas as pd
from sklearn import linear_model
from sklearn.metrics import mean_squared_error

## Read data into correct vectors ##

inputData=pd.read_csv('boston_housing.data', delimiter=' ' , header=None)

X_train = inputData.iloc[:20, :13]
Y_train = inputData.iloc[:20, -1]

X_test = inputData.iloc[400:, :13]
Y_test = inputData.iloc[400:, -1]

## Train linear regression model ##

linearRegressionModel = linear_model.LinearRegression()
linearRegressionModel.fit(X_train, Y_train)
Y_pred = linearRegressionModel.predict(X_test)

# Print results for linear regression model #

print("")
print("----- Results (linear regression) -----")
print("Mean squared error: %.2f" % mean_squared_error(Y_test, Y_pred))
print("")

print("Coefficients:")
print(linearRegressionModel.coef_)
print("")

## Train ridge regression model ##

ridgeRegressionModel = linear_model.RidgeCV (alphas=[0.01, 0.1, 1.0, 10.0, 100.0], cv=5)
ridgeRegressionModel.fit (X_train, Y_train)
Y_pred = ridgeRegressionModel.predict(X_test)

# Print results for ridge regression model #
print("----- Results (ridge regression) -----")
print("Mean squared error: %.2f" % mean_squared_error(Y_test, Y_pred))
print('Alpha: %d' % ridgeRegressionModel.alpha_ )
print("")

print("Coefficients:")
print(ridgeRegressionModel.coef_)
print("")

