# Trevor Nierman
# R957F692
# CS697 HW 3

# knnhw3.py

import pandas 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import mean_squared_error

## Read input data into correct vectors ##

inputData = pandas.read_csv('zip.train', delim_whitespace=True, skipinitialspace=True, header=None)

xTrain = inputData.iloc[:, 1:]
yTrain = inputData.iloc[:, 0]

inputData = pandas.read_csv('zip.test', delim_whitespace=True, skipinitialspace=True, header=None)

xTest = inputData.iloc[:, 1:]
yTest = inputData.iloc[:, 0]

## Train k-nearest neighbor model ##

kn3Model = KNeighborsClassifier(n_neighbors=3)
kn5Model = KNeighborsClassifier(n_neighbors=5)
kn7Model = KNeighborsClassifier(n_neighbors=7)

kn3Model.fit(xTrain, yTrain)
kn5Model.fit(xTrain, yTrain)
kn7Model.fit(xTrain, yTrain)

## Predict results ##

kn3Pred = kn3Model.predict(xTest)
kn5Pred = kn5Model.predict(xTest)
kn7Pred = kn7Model.predict(xTest)

## Print results ##

print("")
print("----- Results: -----")
print("")

error = 0
for i in range(len(kn3Pred)):
    if kn3Pred[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(kn3Pred))

classificationError = (error/totalClassifications) * 100

print("3 nearest neighbors prediction: ")
print(kn3Pred)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f percent" % classificationError)
print("")


error = 0
for i in range(len(kn5Pred)):
    if kn5Pred[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(kn5Pred))

classificationError = (error/totalClassifications) * 100

print("5 nearest neighbors prediction: ")
print(kn5Pred)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f percent" % classificationError)
print("")


error = 0
for i in range(len(kn7Pred)):
    if kn7Pred[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(kn7Pred))

classificationError = (error/totalClassifications) * 100

print("7 nearest neighbors prediction: ")
print(kn7Pred)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f percent" % classificationError)
print("")


