# Trevor Nierman
# R957F692
# CS697 HW3

# rfhw3.py

import pandas
from sklearn.ensemble import RandomForestClassifier

## Read input data into correct vectors ##

inputData = pandas.read_csv('zip.train', delim_whitespace=True, skipinitialspace=True, header=None)

xTrain = inputData.iloc[:, 1:]
yTrain = inputData.iloc[:, 0]

inputData = pandas.read_csv('zip.test', delim_whitespace=True, skipinitialspace=True, header=None)

xTest = inputData.iloc[:, 1:]
yTest = inputData.iloc[:, 0]

## Train forest models ##

forest1 = RandomForestClassifier(n_estimators=1)
forest10 = RandomForestClassifier(n_estimators=10)
forest100 = RandomForestClassifier(n_estimators=100)
forest1000 = RandomForestClassifier(n_estimators=1000)

forest1 = forest1.fit(xTrain, yTrain)
forest10 = forest10.fit(xTrain, yTrain)
forest100 = forest100.fit(xTrain, yTrain)
forest1000 = forest1000.fit(xTrain, yTrain)

## Predict results ##

yPred1 = forest1.predict(xTest)
yPred10 = forest10.predict(xTest)
yPred100 = forest100.predict(xTest)
yPred1000 = forest1000.predict(xTest)

## Print results ##

print("")
print("----- Results: -----")
print("")

error = 0
for i in range(len(yPred1)):
    if yPred1[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred1))

classificationError = (error/totalClassifications) * 100

print("Forest with n_estimators=1: ")
print(yPred1)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")


error = 0
for i in range(len(yPred10)):
    if yPred10[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred10))

classificationError = (error/totalClassifications) * 100

print("Forest with n_estimators=10: ")
print(yPred10)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")


error = 0
for i in range(len(yPred100)):
    if yPred100[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred100))

classificationError = (error/totalClassifications) * 100

print("Forest with n_estimators=1: ")
print(yPred100)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")


error = 0
for i in range(len(yPred1000)):
    if yPred1000[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred1000))

classificationError = (error/totalClassifications) * 100

print("Forest with n_estimators=1: ")
print(yPred1000)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")
