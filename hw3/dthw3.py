# Trevor Nierman
# R957F692
# CS697 HW3

# dthw3.py

import pandas
from sklearn import tree

## Read input data into correct vectors ##

inputData = pandas.read_csv('zip.train', delim_whitespace=True, skipinitialspace=True, header=None)

xTrain = inputData.iloc[:, 1:]
yTrain = inputData.iloc[:, 0]

inputData = pandas.read_csv('zip.test', delim_whitespace=True, skipinitialspace=True, header=None)

xTest = inputData.iloc[:, 1:]
yTest = inputData.iloc[:, 0]

## Train decision tree models ##

tree1 = tree.DecisionTreeClassifier(min_samples_leaf=1)
tree3 = tree.DecisionTreeClassifier(min_samples_leaf=3)
tree5 = tree.DecisionTreeClassifier(min_samples_leaf=5)
tree7 = tree.DecisionTreeClassifier(min_samples_leaf=7)

tree1 = tree1.fit(xTrain, yTrain)
tree3 = tree3.fit(xTrain, yTrain)
tree5 = tree5.fit(xTrain, yTrain)
tree7 = tree7.fit(xTrain, yTrain)

## Predict results ##

yPred1 = tree1.predict(xTest)
yPred3 = tree3.predict(xTest)
yPred5 = tree5.predict(xTest)
yPred7 = tree7.predict(xTest)

## Print results ##

print("")
print("----- Results: -----")
print("")

error = 0
for i in range(len(yPred1)):
    if yPred1[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred1))

classificationError = (error/totalClassifications) * 100

print("Tree with min_samples_leaf=1: ")
print(yPred1)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")


error = 0
for i in range(len(yPred3)):
    if yPred3[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred3))

classificationError = (error/totalClassifications) * 100

print("Tree with min_samples_leaf=3: ")
print(yPred3)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")


error = 0
for i in range(len(yPred5)):
    if yPred5[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred5))

classificationError = (error/totalClassifications) * 100

print("Tree with min_samples_leaf=5: ")
print(yPred5)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")


error = 0
for i in range(len(yPred7)):
    if yPred7[i] != yTest[i]:
        error += 1

error = float(error)
totalClassifications = float(len(yPred7))

classificationError = (error/totalClassifications) * 100

print("Tree with min_samples_leaf=7: ")
print(yPred7)
print("Number of misclassifications: %d" % error)
print("Total number of classifications: %d" % totalClassifications)
print("Percentage misclassified (classification error): %0.2f" % classificationError)
print("")
